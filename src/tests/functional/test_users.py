import json

from src.api.models import User


def test_add_user(test_client, test_database):
    response = test_client.post(
        "/users",
        data=json.dumps({"username": "tim", "email": "tim@email.com"}),
        content_type="application/json",
    )
    data = json.loads(response.data.decode())
    assert response.status_code == 201
    assert "tim@email.com was added" in data["message"]


def test_add_user_invalid_json(test_client, test_database):
    response = test_client.post(
        "/users", data=json.dumps({}), content_type="application/json"
    )
    data = json.loads(response.data.decode())
    assert response.status_code == 400
    assert "Input payload validation failed" in data["message"]


def test_add_user_invalid_json_keys(test_client, test_database):
    response = test_client.post(
        "/users",
        data=json.dumps({"email": "tim@geemail.com"}),
        content_type="application/json",
    )
    data = json.loads(response.data.decode())
    assert response.status_code == 400
    assert "Input payload validation failed" in data["message"]


def test_add_user_duplicate_email(test_client, test_database, add_user):
    add_user(username="Timmeh", email="timmeh@geemail.com")
    response = test_client.post(
        "/users",
        data=json.dumps({"username": "Timmeh", "email": "timmeh@geemail.com"}),
        content_type="application/json",
    )

    data = json.loads(response.data.decode())
    assert response.status_code == 400
    assert "Sorry. That email already exists." in data["message"]


def test_single_user(test_client, test_database, add_user):
    user = add_user(username="Io", email="io@catmail.com")
    response = test_client.get(f"/users/{user.id}")
    data = json.loads(response.data.decode())
    assert response.status_code == 200
    assert "Io" in data["username"]
    assert "io@catmail.com" in data["email"]


def test_single_user_incorrect_id(test_client, test_database):
    response = test_client.get("/users/666")
    data = json.loads(response.data.decode())
    assert response.status_code == 404
    assert "User 666 does not exist" in data["message"]


def test_no_users(test_client, test_database):
    test_database.session.query(User).delete()
    response = test_client.get("/users")

    assert response.status_code == 200


def test_all_users(test_client, test_database, add_user):
    test_database.session.query(User).delete()
    add_user(username="Sol", email="sol@email.com")
    add_user(username="Tim", email="tim@email.com")

    response = test_client.get("/users")
    data = json.loads(response.data.decode())

    assert len(data) == 2
    assert response.status_code == 200
    assert "Sol" in data[0]["username"]
    assert "Tim" in data[1]["username"]
    assert "sol@email.com" in data[0]["email"]
    assert "tim@email.com" in data[1]["email"]
